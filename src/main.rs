extern crate rand;
use rand::Rng;
use std::io;
use std::cmp::Ordering;

fn main(){

  let numero_secreto:u8 = rand::thread_rng().gen_range(1..100);
  let mut tentativas:u32 = 0;

  // println!("o número secreto é: {}", numero_secreto);

  println!("Jogo de adivinhação digite um numero de 1 - 100");
  println!("Digite o seu palpite");

  loop{

    tentativas += 1;

    // Gerando o palpite.

    let mut palpite = String::new();

    io::stdin().read_line(&mut palpite)
        .expect("Erro ao ler a msg.");

    let palpite: u8 = match palpite.trim().parse(){
      Ok(num) => num,
      Err(_) => continue
    };

    // Processando o palpite

    match palpite.cmp(&numero_secreto) {
      Ordering::Greater => println!("O valor é muito alto."),
      Ordering::Less => println!("O valor é muito baixo."),
      Ordering::Equal => {
        println!("Parabéns você acertou com {} tentativas!!", tentativas);
        break;
      }
    }
  }
}
